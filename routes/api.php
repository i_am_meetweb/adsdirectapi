<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'user', 'middleware' => 'apiToken'], function (){
    Route::get('/', [
        'uses' => 'UserController@index',
        'as' => 'user'
    ]);

    Route::get('/by_id', [
        'uses' => 'UserController@index',
        'as' => 'user'
    ]);
});

Route::group(['prefix' => 'media_house', 'middleware' => 'apiToken'], function(){
	Route::get('/', 'MediaHouseController@index');

	Route::get('/{name}', 'MediaHouseController@getSlotsByMediaHouseName');

});

Route::group(['prefix' => 'category', 'middleware' => 'apiToken'], function(){
	Route::get('/', 'CategoryController@index');

	Route::get('/{name}', 'CategoryController@getSlotsByCategoryName');

	Route::get('/{name}/subcategory', 'CategoryController@getSubCategoryList');

    Route::get('/{category}/{sub}', 'CategoryController@getSlotsBySubCategoryName');

});

Route::group(['prefix' => 'adslot', 'middleware' => 'apiToken'], function (){
    Route::get('/',[
        'uses' => 'AdslotController@index',
        'as' => 'adslot'
    ]);

    Route::get('/{id}',[
        'uses' => 'AdslotController@getOneAdSlot',
        'as' => 'adslot.getOne'
    ]);
});

Route::post('register', 'Auth\RegisterController@newUser');
Route::post('login', 'Auth\LoginController@login');