<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notify')->comment('The user to be notified');
            $table->unsignedInteger('by')->comment('The person who makes the notification');
            $table->string('content')->comment('Message to the user that receives notification.');
            $table->boolean('read')->default(false)->comment('Boolean: Set to true if read');
            $table->integer('type')->comment('0 - Users, 1 - Booking');
            $table->timestamps();

            $table->foreign('notify')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
