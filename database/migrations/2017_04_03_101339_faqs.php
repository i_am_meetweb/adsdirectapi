<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//use DB;

class Faqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('faqs',function(Blueprint $table){
            $table->increments('faq_id');
            $table->string('question',300)->nullable();
            $table->text('answer')->nullable();
//            $table->string('slug',300)->nullable();
            $table->timestamps();
        });

        //DB::statement('ALTER TABLE `faqs` ADD FULLTEXT(`answer`,`question`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
