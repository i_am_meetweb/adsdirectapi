<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserRolePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_role_id');
            $table->unsignedInteger('permission_id');

            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade');
            $table->foreign('user_role_id')->references('id')->on('user_role')
                ->onUpdate('cascade');
            $table->timestamps();
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_role_permissions');    }
}
