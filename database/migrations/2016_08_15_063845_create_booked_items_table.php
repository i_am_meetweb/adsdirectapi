<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookedItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::create('booked_items', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('booking_id');
                $table->unsignedInteger('ad_slot_id');
                $table->double('discount_price', 12, 2)->default(0.00);
                $table->double('savings', 12, 2);
                $table->double('tax', 12, 2);
                $table->double('service', 12, 2);
                $table->double('total_discount', 12, 2);
                $table->integer('status');
                $table->string('title')->nullable();
                $table->string('media')->nullable();
                $table->string('caption')->nullable();
                $table->float('subtotal');
                $table->timestamp('time_to_publish')->nullable();
                $table->timestamp('published_at')->nullable();
                $table->dateTime('canceled_at')->nullable();
                $table->double('cancelable_period', 20,4)->comment('Convert the number of days or weeks to a '
                    . 'decimal time in hours');

                $table->timestamps();

                $table->foreign('booking_id')->references('id')->on('bookings')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booked_items');
    }
}
