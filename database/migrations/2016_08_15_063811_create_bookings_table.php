<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->float('total_cost')->default(0);
            $table->unsignedInteger('status')->default(0);
            $table->boolean('payed_for')->default(false);
            $table->unsignedInteger('buyer_id');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->string('type')->default('General');
            $table->timestamps();

            $table->foreign('buyer_id')->references('id')->on('users')->onUpdate('cascade');
            $table->string('transaction_reference')->nullable();
            $table->text('charges')->nullable();
            $table->float('total_cost', 12, 2)->change();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
