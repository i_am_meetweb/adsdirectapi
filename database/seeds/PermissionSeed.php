<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->permissions() as $permission => $actions){
            foreach ($actions as $action){
                Permission::create($action);
            }
        }
    }

    public function permissions()
    {
        return [
            "manage inventory" => [

                "create_adslots" => [
                    "name" => "Create Adslots",
                    "slug" => "create_adslots",
                    "description" => "permission to create adslots",
                ],

                "update_adslots" => [
                    "name" => "Update Adslots",
                    "slug" => "update_adslots",
                    "description" => "permission to update/edit adslots",
                ],

                "delete_adslots" => [
                    "name" => "Delete Adslots",
                    "slug" => "delete_adslots",
                    "description" => "permission to delete adslots",
                ],

                "view_adslots" => [
                    "name" => "View Adslots",
                    "slug" => "view_adslots",
                    "description" => "permission to view slots, applies to merchant",
                ],
                "approve_adslots" => [
                    "name" => "Approve Adslots",
                    "slug" => "approve_adslots",
                    "description" => "permission to approve slots, applies to merchant",
                ],
            ],
            "bookings" => [
                "view_bookings" => [
                    "name" => "View Bookings",
                    "slug" => "view_bookings",
                    "description" => "permission to view bookings",
                ],
                "publish_bookings" => [
                    "name" => "Publish Bookings",
                    "slug" => "publish_bookings",
                    "description" => "permission to publish bookings, applies to merchant",
                ],
                "update_bookings" => [
                    "name" => "Update Bookings",
                    "slug" => "update_bookings",
                    "description" => "permission to update/edit bookings",
                ],
                "delete_bookings" => [
                    "name" => "Delete Bookings",
                    "slug" => "delete_bookings",
                    "description" => "permission to delete bookings",
                ],
            ],
            "media houses" => [
                "create_mediahouses" => [
                    "name" => "Create Mediahouses",
                    "slug" => "create_mediahouses",
                    "description" => "permission to create Mediahouses",
                ],
                "update_mediahouses" => [
                    "name" => "Update Mediahouses",
                    "slug" => "update_mediahouses",
                    "description" => "permission to update/edit Mediahouses",
                ],
                "delete_mediahouses" => [
                    "name" => "Delete Mediahouses",
                    "slug" => "delete_mediahouses",
                    "description" => "permission to delete Mediahouses",
                ],
                "view_mediahouses" => [
                    "name" => "View Mediahouses",
                    "slug" => "view_mediahouses",
                    "description" => "permission to view Mediahouses",
                ],
            ],
            "dashboard" => [
                "view_dashboard" => [
                    "name" => "View Dashboard",
                    "slug" => "view_dashboard",
                    "description" => "permission to view Dashboard, applies to merchant",
                ],
            ]
        ];
    }
}
