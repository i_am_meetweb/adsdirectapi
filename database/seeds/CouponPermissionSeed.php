<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class CouponPermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $couponPermissions = [
            'create' => [
                'name' => 'Create Coupon',
                'slug' => 'create_coupon',
                'description' => 'Create a new coupon'
            ],
            'edit' => [
                'name' => 'Edit Coupon',
                'slug' => 'edit_coupon',
                'description' => 'Edit coupon that is already created.'
            ],
            'view' => [
                'name' => 'View Coupon',
                'slug' => 'view_coupon',
                'description' => 'View coupon'
            ],
            'Delete' => [
                'name' => 'Delete Coupon',
                'slug' => 'delete_coupon',
                'description' => 'Permanently delete coupon'
            ],
            'Enable' => [
                'name' => 'Enable Coupon',
                'slug' => 'enable_coupon',
                'description' => 'Enable coupon for use as a discount'
            ]
        ];

        \DB::transaction(function () use ($couponPermissions) {
            foreach ($couponPermissions as $inner_key => $permissions) {
                Permission::create([
                    "name" => $permissions['name'],
                    "slug" => $permissions['slug'],
                    "description" => $permissions['description'],
                ]);
            }
        });
    }
}
