<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles() as $key => $role) {
            Role::create([
                "name" => $role['name'],
                "slug" => $key,
                "description" => $role['description'],
                "discount" => $role['discount'],
                "purchase_discount" => $role['purchase_discount'],
                "applies_to" => $role['applies_to'],
            ]);
        }
    }

    public function roles()
    {
        return [
            "advertiser" => [
                "name" => "Advertiser",
                "slug" => "advertiser",
                "description" => "the default User role",
                "applies_to" => "alpha",
                "discount" => 10,
                "purchase_discount" => 5,
            ],
            "merchant" => [
                "name" => "Merchant",
                "slug" => "merchant",
                "description" => "this are user who register media house they manage",
                "applies_to" => "alpha",
                "discount" => 0,
                "purchase_discount" => 0,
            ],
            "aso_rock" => [
                "name" => "Super Administrator",
                "description" => "the user role",
                "applies_to" => "alpha",
                "discount" => 0,
                "purchase_discount" => 0,
            ],
            "admin" => [
                "name" => "Administrator",
                "description" => "the admin role",
                "applies_to" => "alpha",
                "discount" => 0,
                "purchase_discount" => 0,
            ],
            "advert_executive" => [
                "name" => "Agency",
                "description" => "the default User role",
                "applies_to" => "alpha",
                "discount" => 15,
                "purchase_discount" => 0,
            ],
            "sales_executive" => [
                "name" => "Sales Executive",
                "description" => "the default sales exec role",
                "applies_to" => "merchant",
                "discount" => 0,
                "purchase_discount" => 0,
            ],
            "account_executive" => [
                "name" => "Account Executive",
                "description" => "the default account exec role",
                "applies_to" => "merchant",
                "discount" => 15,
                "purchase_discount" => 0,
            ],
            "mechant_admin" => [
                "name" => "Merchant Administrator",
                "description" => "the default merchant Admin role",
                "applies_to" => "merchant",
                "discount" => 0,
                "purchase_discount" => 0,
            ]
        ];

    }
}
