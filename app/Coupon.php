<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CouponUsed;

class Coupon extends Model
{
    protected $fillable = ['name', 'slug', 'discount', 'status'];

    /**
    * Check if the coupon is used by the user
    * @return boolean
    */
    public function isUsed(){
    	if($this->coupon()->first()){
    		return true;
    	}else{
    		return false;
    	}
    }

    /**
    * Set coupon as used
    */
    public function markUsed(){
    	$this->coupon(true);
    }

    /**
    * Get the coupon from the table of used coupons
    * The coupen will be determined by finding from the list of used coupons
    * for coupon that matches the same coupon_id and the user_id
    * @param $using boolean Set to true to mark the coupon as used
    */
    public function coupon($using = false){
    	/**
    	* Getting the coupon id
    	*/
    	$coupon_id = $this->id;

    	/**
    	* Getting the current user id
    	*/
    	$user_id = auth()->check() ? auth()->user()->id : 0;

    	if($using){
    		return CouponUsed::insert([
    			'coupon_id' => $coupon_id,
    			'user_id' => $user_id
    		]);
    	}

    	return CouponUsed::where('coupon_id', $coupon_id)
    							->where('user_id', $user_id);
    }
}
