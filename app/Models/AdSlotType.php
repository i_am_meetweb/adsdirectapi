<?php

namespace App\Models;

use App\Models\AdSlot;
use Illuminate\Database\Eloquent\Model;

class AdSlotType extends Model
{
    protected $table = 'ad_slots_type';
	protected $fillable = [
		'name'
	];

   
}
