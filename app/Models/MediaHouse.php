<?php

namespace App\Models;

use App\Models\Audience;
use App\Models\Focus;
use App\Models\MerchantMediaHouse;
use App\User;
use App\UserRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaHouse extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'name',
    	'description',
    	'location',
    	'owner_id',
        'slug'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public static function withSlug($slug)
    {
        return static::whereSlug($slug)->firstOrFail();
    }

    public static function getImageBaseUrl($path, $image, $ext="png"){
    $baseurl = "https://s3-us-west-2.amazonaws.com/adsdirect/assets";
        $ext = ($ext == '')? '' : ".".$ext;
        $path = ($path == '')? '' : "/".$path;

        return $baseurl.$path."/".$image.$ext;
    }
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function owner()
    {
    	return $this->belongsTo(User::class, 'owner_id');
    }

     public function merchantowner()
    {
        return $this->belongsTo(UserRole::class, 'owner_id');
    }

    

    public function adSlots()
    {
        return $this->hasMany(AdSlot::class, 'media_house_id');
    }
    public function audiences()
    {
        return $this->belongsToMany(Audience::class, 'audience_media_house', 'media_house_id', 'audience_id');
    }

    public function focuses()
    {
        return $this->belongsToMany(Focus::class, 'focus_media_house', 'media_house_id', 'focus_id')->withTimestamps();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_media_house', 'media_house_id', 'cat_id')->withTimestamps();
    }

    public function subCategories()
    {
        return $this->belongsToMany(SubCategory::class, 'ad_slots', 'media_house_id', 'sub_cat_id');
    }

    public function slotTypes()
    {
        return  $this->adSlots()->distinct()->get(['type'])->pluck('type');
    }

    public function isVerified(){
        return $this->is_verified;
    }

     public function assignToUserRole($userole)
    {
      //  $this->hasUser($user);
             // dd($user);
            return $this->userroles()->save($userole);
        //throw new \Exception("Already has permission {$this->name}");
    }

    public function userroles()
    {
        return $this->belongsToMany(UserRole::class, 'merchant_media_house', 'media_house_id', 'user_role_id')->withTimestamps();
    }


    public function assignMediaHouse($mediahouse, $role)
    {
        $mediahouse->assignToUserRole($role);
    }



}
