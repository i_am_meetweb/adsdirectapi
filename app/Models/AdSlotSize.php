<?php

namespace App\Models;

use App\Models\AdSlot;
use Illuminate\Database\Eloquent\Model;

class AdSlotSize extends Model
{
    protected $table = 'ad_slots_size';
	protected $fillable = [
		'name', 'unit'
	];

   
}
