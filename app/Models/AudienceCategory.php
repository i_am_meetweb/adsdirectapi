<?php

namespace App\Models;

use App\Models\Audience;
use Illuminate\Database\Eloquent\Model;

class AudienceCategory extends Model
{
    protected $fillable = [
    	'name',
    	'slug',
    	'description',
    ];
    
    public function audiences()
    {
    	return $this->hasMany(Audience::class, 'audience_category_id');
    }
}
