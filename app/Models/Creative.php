<?php 

namespace App\Models;

use Storage;
/**
 * A model for creatives
 */
class Creative
{
	protected $path;
	protected $parts = [];

    /**
     * Create an instance of a creative
     */
    public function __construct($path)
    {
		$this->path = $path;
		$this->parts = $this->getParts();
		$this->size = $this->getSize();
    }
    public static function make($path)
    {
    	return new static($path);
    }

    public function getName()
    {
    	return $this->parts['basename'];
    }

    public function fullPath()
    {
    	return Storage::url($this->path);
    }

    public function getSignedUrl()
    {
    	$s3 = Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $bucket = config('filesystems.disks.s3.bucket');

        $command = $client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $this->path,
        ]);

        $request = $client->createPresignedRequest($command, '+20 minutes');

        return (string) $request->getUri();
    }

    public function type()
    {
        
    }

    public function mimeType($type)
    {
    		// 
    }
    public function extension()
    {
    	return $this->parts['extension'];
    }

    public function getSize()
    {
    	return Storage::size($this->path);
    }

    public function getParts()
    {
    	return pathinfo($this->path);
    }
}