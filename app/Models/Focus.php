<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Focus extends Model
{
	protected $table = "focuses";
    protected $fillable = [
    	'name',
    	'description',
    ];
    public function mediaHouses()
    {
    	return $this->belongsToMany(MediaHouse::class, 'audience_media_house', 'audience_id', 'media_house_id')->withTimestamps();
    }
}
