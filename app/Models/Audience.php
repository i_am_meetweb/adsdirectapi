<?php

namespace App\Models;

use App\Models\AudienceCategory;
use Illuminate\Database\Eloquent\Model;

class Audience extends Model
{
	protected $fillable = [
		'name',
		'description',
	];

    public function mediaHouses()
    {
    	return $this->belongsToMany(MediaHouse::class, 'audience_media_house', 'audience_id', 'media_house_id')->withTimestamps();
    }
    public function category()
    {
    	return $this->belongsTo(AudienceCategory::class,'audience_category_id');
    }
}
