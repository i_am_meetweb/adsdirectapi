<?php

namespace App\Models;

use App\UserRole;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Permission extends Model
{

    protected $fillable = [
        'name',
        'slug',
        'description'
    ];


    public static function withSlug($value)
    {
        $minutes = Carbon::now()->addMinutes(5);
        return Cache::remember('user:permission:slug' . $value, $minutes, function () use ($value) {
            return static::whereSlug($value)->first();
        });
    }

    /**
     * Assign a role to a user
     * @param $user
     * @return Model
     * @throws \Exception
     */
    public function assignToUserRole($userole)
    {
        //  $this->hasUser($user);
        //dd($userole);
        return $this->users()->save($userole);


        //throw new \Exception("Already has permission {$this->name}");
    }

    public function users()
    {
        return $this->belongsToMany(UserRole::class, 'user_role_permissions', 'permission_id', 'user_role_id')->withTimestamps();
    }

    public function assignPermission($permission, $role)
    {
        $permission->assignToUserRole($role);
    }

    public function hasUser($user)
    {
        // $user = $user instanceof User ? $user->getKey() : $user;
        // $this->users()->wherePivot('user_role_id', '=', $user)->count() > 0;
        // exit();
    }

    // public function seedRolePermission($role_slug, $permission_slug){
    // 	 $role = Role::class->where('slug', $role_slug)->first();
    // 	 $permission = Role::class->where('slug', $permission_slug)->first();

    //      $role->id; 

    //       $flight = new Flight;

    //     $role->name = $request->name;

    //     $flight->save(); 
    // }
}
 