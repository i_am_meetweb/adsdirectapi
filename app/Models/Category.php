<?php

namespace App\Models;

use App\Models\AdSlot;
use App\Models\MediaHouse;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
    	'name',
    	'slug',
    	'description',
    ];
    public function mediaHouses()
    {
    	return $this->belongsToMany(MediaHouse::class,'category_media_house', 'cat_id', 'media_house_id');
    }
    
    public function subCategories()
    {
    	return $this->hasMany(SubCategory::class, 'cat_id');
    }

    public function adSlots()
    {
        return $this->hasManyThrough(AdSlot::class, SubCategory::class, 'cat_id','sub_cat_id');
    }
    
    public function adSlotsType()
    {
        return $this->adSlot()->get(["id", "type"]);
    }
}
