<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryMediaHouse extends Model
{
    //
    protected $table = 'category_media_house';
    protected $fillable = ['cat_id','media_house_id'];
}
