<?php
/**
 * Created by PhpStorm.
 * User: maxv
 * Date: 8/4/2016
 * Time: 9:00 AM
 */

namespace App\Models\Traits;


use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

trait HasRole {

	public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role','user_id', 'role_id');
    }

    public function role()
    {
        return $this->roles()->first();
    }
    
    public function hasRole($roles = [])
    {
        if (is_string($roles) || $roles instanceof Role){
            $roles = func_get_args();
        }
        if (count($roles) > 0){
            $roles = Collection::make($roles)->map(function($role){
                return $role instanceof Role ? $role : Role::withSlug($role);
            }, $roles)->filter(function($value){
                return !is_null($value);
            });
        	return $this->roles()->wherePivotIn('role_id', $roles->modelKeys())->count() > 0;
    	}
    	return false;
    }

    public function assignRole($role)
    {
        $role = $role instanceof Role ? $role : Role::defaultRole();
        
        $role->assignToUser($this);
    }

    public function updateRole($role)
    {
        $role = $role instanceof Role ? $role : Role::defaultRole();
        
        if($this->hasAlreadyARole()){
            $this->roles()->detach();
        }

        $role->assignToUser($this);
    }

    public function scopeWithRole($query, $role)
    {
        $role = is_string($role) ? $role : $role->name;
        return $query->whereHas('roles', function($query) use ($role){
            $query->where('name', $role);
        }); 
    }

    public function hasAlreadyARole()
    {
        return $role = $this->roles()->count() > 0;
    }
}