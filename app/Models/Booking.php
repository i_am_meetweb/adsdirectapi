<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'total_cost',
        'tax',
        'service',
        'nameOfOrganisation',
        'total_discount',
        'savings',
    	'status',
    	'type',
    	'payed_for',
    	'start_date',
    	'end_date',
    	'buyer_id',
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'start_date', 'end_date'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'payed_for' => 'boolean',
    ];

    public function buyer()
    {
    	return $this->belongsTo(User::class, 'buyer_id');
    }
    public function getRefNoAttribute()
    {
        return sprintf("BK%s%d", $this->created_at->format('Ymd'), $this->id);
    }
 
    public function bookedItems()
    {
        return $this->hasMany(BookedItem::class, 'booking_id');
    }

    public function getTotalQuantityAttribute()
    {
        $tQ = $this->bookedItems->sum('quantity');
        return $tQ;   
    }

    public function incorpOfTrusteesNames(){
        return $this->hasMany('App\IOTName', 'booking_id');
    }

    public function getPaidForAttribute()
    {
        return $this->payed_for;
    }

    public function getTotalItemsAttribute()
    {
        return $this->bookedItems()->count();
    }

    public static function idFromRefNo($refNo)
    {
        return substr($refNo, 10);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'ref_no';
    }

    /**
     * Scope for retrieving order based on reference no
     * 
     * @param  \Ilumninate\Database\Eloquent\Builder  $query
     * @param  string  $refNo
     * @return  \Ilumninate\Database\Eloquent\Builder
     */
    public function scopeWithRefNo($query, $refNo)
    {
        return $query->where('id', static::idFromRefNo($refNo));
    }

    public function scopeUnPaid($query)
    {
        return $query->where('payed_for', false);
    }

    public function scopePaid($query)
    {
        return $query->where('payed_for', true);
    }
}
