<?php

namespace App\Models;

use App\Appearance;
use App\Currency;
use App\Models\Category;
use App\Models\BookedItem;
use App\Models\MediaHouse;
use App\Models\SubCategory;
use App\Size;
use App\Type;
/**
 * Uncomment the below lines to use MySQL
 */
//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Uncomment the below lines to use MongoDB
 */
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class AdSlot extends Model
{
    use SoftDeletes;

    /**
     * MongoDB Configuration
     * @var string $connection
     * @var string $collection
     */
    protected $connection = 'mongodb';
    protected $collection = 'ad_slots';

    private static $baseurl = "https://s3-us-west-2.amazonaws.com/adsdirect/assets/";

    protected $fillable = [
        'name',
        'type_id',
        'description',
        'sub_cat_id',
        'media_house_id',
        'size_id',
        'appearance_id',
        'price',
        'currency_id',
        'cost_factor',
        'location',
        'media_type',
        'duration',
        'available_start_time',
        'available_duration',
        'cancelable_period'
    ];

     public static function getImageBaseUrl($path = null)
     {
         return self::getBaseUrl() . $path;
     }

     private static function getBaseUrl()
     {
         return self::$baseurl;
     }

    public function getRefNoAttribute()
    {
        return sprintf("AD%s%d", $this->created_at->format('Ymd'), $this->id);
    }

    public static function idFromRefNo($refNo)
    {
        return substr($refNo, 10);
    }

     public function getSize()
     {
         return $this->size->name . ' ' . $this->size->unit;
     }

    public function getPrice()
    {
        return $this->price + ($this->price * $this->discount) / 100;
    }

    public function bookedItem()
    {
        return $this->hasMany(BookedItem::class, 'ad_slot_id');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'ref_no';
    }
}
