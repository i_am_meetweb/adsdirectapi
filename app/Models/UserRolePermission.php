<?php

namespace App\Models;

use App\Models\Permission;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class UserRolePermission extends Model
{

    protected $fillable =[
    	'user_role_id',
      'permission_id'
    ];
    

}
 