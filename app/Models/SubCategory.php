<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
   protected $fillable = [
    	'name',
    	'slug',
    	'description',
    ];

    public function slots()
    {
    	return $this->hasMany(AdSlot::class, 'sub_cat_id');
    }
    public function category()
    {
    	return $this->belongsTo(Category::class, 'cat_id');
    }
}
