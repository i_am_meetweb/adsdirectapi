<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Role extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $hidden = ['pivot', 'deleted_at', 'updated_at'];

	protected $fillable = [
		'name',
		'slug',
		'description',
		'purchase_discount',
        'discount',
        'applies_to'
	];

	public function setSlugAttribute($value)
	{
		$this->attributes['slug'] =Str::slug($value, '_');
	}

	public function setNameAttribute($value)
	{
		$this->attributes['name'] = strtolower($value);
	}

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_role', 'role_id', 'user_id')->withTimestamps();
    }

    public function withId($id)
    {
    	$minutes = Carbon::now()->addMinutes(2);
    	return Cache::remember('user:role:'.$id, $minutes, function() use ($id) {
    		return static::findOrFail($id);
    	});
    	
    }

    public static function assignUserTo($role, $user)
    {
		$roleObject = static::withSlug($role);
		$roleObject->users()->save($user);
    }

	/**
	 * Assign a role to a user
	 * @param $user
	 * @return Model
	 * @throws \Exception
	 */
    public function assignToUser($user)
    {
    	if(!$this->hasUser($user)){
           // dd($user);
			return $this->users()->save($user);
    	}

    	throw new \Exception("Already has role {$this->name}");
    }

    /**
     * Check if a role exist
     * 
     * @param  string  $value
     * @return  bool
     */
    public static function slugExists($value)
    {
    	$value = snake_case($value);
    	return static::where('slug', $value)->exists();
    }

    public static function withSlug($value)
    {
        $minutes = Carbon::now()->addMinutes(5);
        return Cache::remember('user:role:slug'.$value, $minutes, function() use ($value) {            
    	   return static::whereSlug($value)->first();
        });
    }

	/**
	 * @param $user
	 * @return bool
	 */
	public function hasUser($user)
	{
		$user = $user instanceof User ? $user->getKey() : $user;
		return $this->users()->wherePivot('user_id', '=', $user)->count() > 0;
	}

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

	public static function defaultRole($value = null)
	{
		return Role::withSlug(config('roles.default', $value));
	}
    
    public function __toString(){
        return $this->name;
    }
}
