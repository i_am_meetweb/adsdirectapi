<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FocusMediaHouse extends Model
{
    //
    protected $table = 'focus_media_house';
    protected $fillable = ['focus_id','media_house_id'];
}
