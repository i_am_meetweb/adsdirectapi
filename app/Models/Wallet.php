<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $table = 'wallet_history';

    protected $primaryKey = 'id';
    
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function partner(){
        return $this->belongsTo(User::class,'partner_id','id');
    }

}
