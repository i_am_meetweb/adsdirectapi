<?php

namespace App\Models;

use App\Models\AdSlot;
use App\Models\Booking;
use App\Models\Creative;
use Illuminate\Database\Eloquent\Model;

class BookedItem extends Model
{
    //Todo: add reduction of bookedItem subtotal from booking when bookedItem is removed
    protected $fillable = [
    	'ad_slot_id',
    	'booking_id',
        'formerName',
        'newName',
        'reasonForChange',
    	'media',
    	'caption',
    	'title',
    	'quantity',
        'start_time',
        'published_url',
        'status',
        'published_img_url',
    ];

    public function setAdSlot($adSlot)
    {
        return $this->ad_slot_id = $adSlot->id;
    }
    public function adSlot()
    {
    	return $this->belongsTo(AdSlot::class, 'ad_slot_id');
    }

    public function booking()
    {
    	return $this->belongsTo(Booking::class, 'booking_id');
    }

    public function creatives()
    {
        return collect($this->getMedia())->filter(function($file){
            if (empty($file)){
                return false;
            }
            return true;

        })->map(function($file){
            return Creative::make($file);
        });
    }
    public function getMedia()
    {
        return json_decode($this->media, true);
    }

    public function setMedia($media)
    {
        return $this->media = json_encode($media);
    }

    public function getRefNoAttribute()
    {
        return sprintf("BK%s%d-%d", $this->created_at->format('Ymd'), $this->booking_id, $this->id);
    }

    public static function idFromRefNo($refNo)
    {
        list(, $id) = explode('-', $refNo);
        return $id;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'ref_no';
    }
    public function unpaid(){
        
    }
}
