<?php

namespace App\Models;

use App\Models\AdSlot;
use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    protected $table = 'notification_type';
	protected $fillable = [
		'name'
	];

   
}
