<?php

namespace App\Models;

use App\Models\AdSlot;
use Illuminate\Database\Eloquent\Model;

class AdSlotAppearance extends Model
{
    protected $table = 'ad_slots_appearance';
	protected $fillable = [
		'name'
	];

   
}
