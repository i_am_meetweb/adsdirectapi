<?php

namespace App\Models;

use App\Models\Audience;
use App\Models\Focus;
use App\User;
use App\Models\MediaHouse;
use App\UserRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantMediaHouse extends Model
{
    protected $table = 'merchant_media_house';
   
    protected $fillable = [
    	'user_role_id',
    	'media_house_id'
            ];


     public function get_media_houses()
    {
        return $this->belongsTo(Mediahouse::class, 'media_house_id');
    }
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
 
}
