<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordRecoveryDB extends Model
{
    //
    protected $table = 'password_recovery';

    protected $fillable = [
        'email',
        'date_generated',
        'recovery_key'
    ];

}
