<?php

namespace App\Listeners;

use App\Events\NewUserEvent;
use App\Models\Role;
use App\UserRole;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignRole
{
    /**
     * Handle the event.
     *
     * @param  NewUserEvent  $event
     * @return void
     */
    public function handle(NewUserEvent $event)
    {
        $user = $event->user;

        UserRole::create([
            'role_id' => Role::where('slug', 'advertiser')->first()->id,
            'user_id' => $user->id
            ]);
    }
}
