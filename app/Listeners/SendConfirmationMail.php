<?php

namespace App\Listeners;

use App\Events\NewUserEvent;
use App\Notifications\NewUserNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendConfirmationMail
{
    use Notifiable;

    /**
     * Create the event listener.
     *
     * @return mixed
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  NewUserEvent  $event
     * @return void
     */
    public function handle(NewUserEvent $event)
    {
        $event->user->notify(new NewUserNotification($event->user));
    }
}
