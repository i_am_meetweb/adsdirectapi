<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IOTName extends Model
{
    protected $table = 'trustees';

    protected $fillable = ['user_id', 'name', 'booking_id'];
}
