<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $table = 'faqs';

    protected $fillable = ['question', 'answer','slug'];

    protected $primaryKey = 'faq_id';
}
