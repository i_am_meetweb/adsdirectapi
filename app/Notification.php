<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\NotificationType;
class Notification extends Model
{
    protected $fillable = ['notify', 'by', 'read', 'content', 'type_id'];

    protected $table = 'notifications';
    public function type()
    {
    	return $this->belongsTo(NotificationType::class, 'type_id');
    }
}
