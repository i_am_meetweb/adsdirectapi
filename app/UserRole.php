<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\MediaHouse;
use App\Models\Traits\HasRole;
use App\Models\Wallet;
use App\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class UserRole extends Model
{
	use HasRole;
  
    protected $table = 'user_role';

    protected $fillable = [
        'role_id', 'user_id'
    ];

}
