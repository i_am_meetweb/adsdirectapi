<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IOT extends Model
{
    protected $table = 'incorp_trustees';
    protected $fillable = ['name_of_org', 'aims', 'file_url', 'status'];

    public function names()
    {
        return $this->hasMany('App\IOTName', 'incorp_trustee_id');
    }

    public function adSlot()
    {
        return $this->hasMany('App\IOTMedia', 'incorp_trustee_id');
    }



}
