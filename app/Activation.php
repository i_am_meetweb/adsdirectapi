<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{

    protected $table ='account_activation';

    protected $fillable = ['email','activation_key','updated_at','created_at','time_generated'];


}
