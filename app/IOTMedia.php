<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IOTMedia extends Model
{
    protected $table = 'incorp_Media';

    protected $fillable = ['adslot_id', 'incorp_trustee_id'];

    /**
     * Relation with AdSlot
     * Reference key adslot_id
     */
    public function slots(){
        return $this->belongsTo('App\Models\AdSlot','adslot_id', 'id');
    }


}
