<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use App\Models\UserRolePermission;
use App\UserRole;
use Closure;
use Illuminate\Support\Facades\Redirect;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission_slug)
    {
        $user_id = auth()->user()->id;
        $user_details = array();

        $user_role = UserRole::where('user_id', $user_id)->first();
        $permissions = UserRolePermission::where('user_role_id', '=', $user_role['id'])->get();
        foreach ($permissions as $value) {
            $selection = Permission::where('id', $value['permission_id'])->first();
            array_push($user_details, $selection['slug']);
        }
        if (!in_array($permission_slug, $user_details) and auth()->user()->role()->slug !== 'aso_rock') {
            return Redirect::back()->with('permission_message', 'you are not permitted to perform this operation');
        }

        return $next($request);
    }
}
