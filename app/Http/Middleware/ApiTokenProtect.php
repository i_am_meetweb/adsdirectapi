<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class ApiTokenProtect extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->header('Authorization')){
            return send_result([],'error','Authorization not supplied!', 401);
        }
        try {
            $user = $this->auth->authenticate(str_replace("Bearer ","",$request->header('Authorization')));
        } catch (TokenExpiredException $e) {
            return send_result([],'error','Token Expired!', 404);
        } catch (JWTException $e) {
            return send_result([],'error','Token Invalid!', 406);
        }

        return $next($request);
    }
}
