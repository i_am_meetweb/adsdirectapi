<?php

namespace App\Http\Controllers;

use App\Adslot;


class AdslotController extends Controller
{
    public function index()
    {
        return send_result(Adslot::paginate(api()->limit), 'success');
    }

    /**
     * Get an adslot by providing its id
     * @param $id int
     *
     * @return mixed
     */
    public function getOneAdSlot($id)
    {
        if(is_null($id)){
            return send_result([], 'error', 'Please check the provided id again!');
        }

        if ($adslot = AdSlot::where('id', $id)->first()) {
            return send_result($adslot, 'success');
        }
        return send_result($adslot, 'error', 'The provided adslot id is not found');

    }
}
