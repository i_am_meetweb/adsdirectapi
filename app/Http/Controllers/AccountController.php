<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Generate alpha-numerical values
     *
     * @param int $length
     * @return string
     */
    public function generateCode($length = 10){
        return str_random($length);
    }

    /**
     * Account activation
     *
     * @param string $mail
     * @param string $code
     *
     * @return mixed
     */
    public function activate(string $mail, string $code){
        if(!$user = $this->account($mail)){
            return "Provided email address is not found on our system!";
        }

        if(!$this->isCodeValid($user, $code)){
            return "The code is not a valid code, please check again!";
        }

        $this->setActivated($user);

        return "Account activated successfully!";
    }

    public function setActivated(User $user){
        $user->account_code = "";
        $user->activated = true;
        $user->save();
    }

    public function isCodeValid(User $user, $code){
        return ((string) $user->account_code == (string) $code);
    }

    /**
     * Get user using email address
     *
     * @param string $mail
     * @return User|boolean
     */
    public function account(string $mail){
        return User::where('email', $mail)->first();
    }


}
