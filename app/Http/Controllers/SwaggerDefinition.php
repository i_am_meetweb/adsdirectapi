<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


/**
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="AdsDirect documented API",
 *     version="1.0.0"
 *   )
 * )
 */
class SwaggerDefinition extends Controller
{}
