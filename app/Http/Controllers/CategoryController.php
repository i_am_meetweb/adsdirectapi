<?php

namespace App\Http\Controllers;

use App\AdSlot;

class CategoryController extends Controller
{
    /**
     * Get all the category lists
     *
     * @return array
     */
    function index()
    {
        return send_result(
            AdSlot::groupBy('category')->get()->pluck(['category']),
            "success"
        );
    }

    /**
     * Get the slots of a category
     *
     * @param string $name The name of the category
     * @return array
     */
    function getSlotsByCategoryName($name)
    {
        $slot = AdSlot::where('category', strtolower($name))->paginate(api()->limit);

        //If the result from the DB is less than or equal to 0, that means the result is not available
        if (count($slot) > 0) {
            return send_result($slot, "success");
        }
        return send_result($slot, "error", "The provided media house name is not found or doesn't have any slot!");
    }

    /**
     * Get the list of sub categories using their category name
     *
     * @param $category_name
     * @return $this
     */
    function getSubCategoryList($category_name)
    {
        $slot = AdSlot::where('category', strtolower($category_name))
            ->groupBy('sub category')
            ->get()
            ->pluck('sub category');

        if (count($slot) < 1) {
            return send_result($slot, 'error', "Something seems wrong with the provided category name and it returned no sub category!");
        }
        return send_result($slot, "success");
    }

    /**
     * Get slot based on their category name and slot name
     *
     * @param $category
     * @param $sub
     * @return $this
     */
    function getSlotsbySubCategoryName($category, $sub)
    {
        $slot = AdSlot::where('category', strtolower($category))
            ->where('sub category', strtolower($sub))
            ->paginate(api()->limit);

        if (count($slot) < 1) {
            return send_result($slot, 'error',
                sprintf("No slot is found for the category (%s) and sub category (%s)!", $category, $sub));
        }
        return send_result($slot, "success");
    }
}
