<?php

namespace App\Http\Controllers\Auth;

use App\Events\NewUserEvent;
use App\User;
use App\Http\Controllers\Controller;
use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'mobile' => 'required',
        ]);
    }

    public function newUser()
    {
        $data = api()->request->all();

        $user = $this->validator($data);

        if(count($user->errors()) > 0){
            return send_result($user->errors(),'error', 'Check the following errors!');
        }

        $user = $this->create($data);

        event(new NewUserEvent($user));

        return send_result($this->createTokenFromUser($user),'success');
    }

    /**
     * Generate token for registered user
     *
     * @param User $user
     * @return JWTAuth
     */
    protected function createTokenFromUser(User $user){
        return JWTAuth::fromUser($user);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => bcrypt($data['password']),
            'account_code' => generate_code()
        ]);
    }


}
