<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TheApiController extends Controller
{

    /**
     * Provide exact number of result
     * @var int limit
     */
    public $limit;


    /**
     * The request object
     */
    public $request;


    /**
     * TheApiController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->limit = $request->get('limit') ? (int)$request->get('limit') : 0;
    }

    /**
     * @param boolean|string $getField The field to get its value from the URI
     * @return mixed|string
     */
    public function get($getField)
    {
        return $this->request->get($getField) ? $this->request->get($getField) : "";
    }

}
