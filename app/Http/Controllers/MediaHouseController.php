<?php

namespace App\Http\Controllers;

use App\AdSlot;

class MediaHouseController extends Controller
{
    /**
     * Get all the media houses
     *
     * @return array
     */
    function index()
    {
        return send_result(
            AdSlot::groupBy('media_house')->get()->pluck(['media_house']),
            'success'
        );
    }

    /**
     * Get the slots of a media house
     *
     * @param string $name The name of the media house
     * @return array
     */
    function getSlotsByMediaHouseName($name)
    {
        $slot = AdSlot::where('media_house', strtoupper($name))->paginate(api()->limit);

        //If the result from the DB is less than or equal to 0, that means the result is not available
        if (count($slot) > 0) {
            return send_result($slot, "success");
        }
        return send_result($slot, "error", "The provided media house name is not found!");
    }
}
