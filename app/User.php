<?php

namespace App;

use App\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = "mysql";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'mobile', 'logins', 'balance', 'activated', 'api_token', 'account_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_by', 'updated_at', 'deleted_at', 'logins', 'account_code','api_token'
    ];

    public function role(){
        return $this->belongsToMany(Role::class, 'user_role','user_id', 'role_id')->first();
    }

}
