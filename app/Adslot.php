<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class AdSlot extends Model
{
    use SoftDeletes;

    /**
     * MongoDB Configuration
     * @var string $connection
     * @var string $collection
     */
    protected $connection = 'mongodb';
    protected $collection = 'ad_slots';


    private static $baseurl = "https://s3-us-west-2.amazonaws.com/adsdirect/assets/";

    protected $fillable = [
        'name',
        'type_id',
        'description',
        'sub_cat_id',
        'media_house_id',
        'size_id',
        'appearance_id',
        'price',
        'currency_id',
        'cost_factor',
        'location',
        'media_type',
        'duration',
        'available_start_time',
        'available_duration',
        'cancelable_period'
    ];

}
