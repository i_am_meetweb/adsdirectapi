<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewUserNotification extends Notification
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @param $user User
     * @return mixed
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('no-reply@adsdirect.com.ng', 'AdsDirect NG')
            ->subject("Account Activation Link")
            ->greeting('Welcome to AdsDirect')
            ->line('In other to start using our service, you have to activate your account by clicking on the below button.')
            ->action('Activate', route('account.activate', [$this->user->email, $this->user->account_code]))
            ->line('Thank you for using our service!');
    }

}
