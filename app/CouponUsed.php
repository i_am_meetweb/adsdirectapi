<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponUsed extends Model
{
	protected $table = 'coupons_used';

	protected $fillables = ['user_id', 'coupon_id'];
}
